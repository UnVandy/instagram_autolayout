//
//  ContentTableViewCell.swift
//  Instagram_Homework
//
//  Created by Un Vandy on 12/1/18.
//  Copyright © 2018 Un Vandy. All rights reserved.
//

import UIKit

class ContentTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heartImageView: UIImageView!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var commentImageView: UIImageView!
    
    @IBOutlet weak var forwardImageView: UIImageView!
    
    @IBOutlet weak var likeLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(instagram: Instagram) {
        nameLabel.text = instagram.name
        likeLabel.text = instagram.like
        statusLabel.text = instagram.status
        
        DispatchQueue.main.async {
            //Use image from url with condition if url no image
            if let url = URL(string: instagram.profile!) {
                if let data = try? Data(contentsOf: url) {
                    if let image = UIImage(data: data) {
                        
                        self.profileImageView.image = image
                        
                    }
                }
            }
            
            if let url = URL(string: instagram.post!) {
                if let data = try? Data(contentsOf: url) {
                    if let image = UIImage(data: data) {
                        
                        self.postImageView.image = image
                        
                    }
                }
            }
            
        }
        
    }
}
